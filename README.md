```
curl -LO https://gitlab.com/jakubolszewski/calm-de/-/raw/master/install.sh && sudo bash install.sh
```
```
cat > /mnt/etc/iwd/main.conf <<"EOF"
...
EOF
```

```
sudo vpm update && sudo vpm cleanup && sudo vpm autoremove && sudo modprobe acpi_call && sudo asus-fan-control set-temps 80 88 90 95 96 97 98 99 && maza update && sudo fstrim / && sudo vkpurge rm all
```

## Roadmap
### Core:
- [ ] Asus fan fix
- [ ] AMD
- [ ] Intel
  - [x] basic firmware
  - [ ] intel-undervolt config
  - [ ] powertop config
  - [ ] tlp config
- [ ] Network
  - [ ] wpa_supplicant
  ```
  #!/usr/bin/env bash
  # If not broadcasting SSID, you'll need to add "scan_ssid=1" into wpa_supplicant.conf
  wpa_passphrase SSID Password | sudo tee -a /etc/wpa_supplicant/wpa_supplicant.conf
  sudo ln -s /usr/share/dhcpcd/hooks/10-wpa_supplicant /usr/libexec/dhcpcd-hooks
  sudo ln -s /etc/sv/dhcpcd /var/service
  sudo sv restart dhcpcd
  ```
    - [ ] wpa_supplicant config
    - [ ] wpa_supplicant settings
- [ ] Bluetooth: bluez
- [ ] Audio: pipewire
- [ ] Touchpad:
- [ ] File system: xfs
### System:
- [ ] System shell: dash
- [ ] Interactive shell: fish
- [ ] Privilege elevation: doas
- [ ] Dark theme: Aurora

- [ ] Light theme: Rose Pine
- [ ] Font Sans-serif: Inter
- [ ] Font Serif: Baskerville
- [ ] Font Monospace: Hack
- [ ] Font Emoji: JoyPixels
- [ ] Font Misc:
### User:
- [ ] File Manager: lf/broot/pistol
- [ ] Brightness Control: light
- [ ] Password manager: pass
- [ ] Browser: qutebrowser, w3m
- [ ] DNS blocker: maza
- [ ] Clipboard Managers: wl-clipboard
- [ ] Compositors: river
- [ ] Display Configuration: kanshi
- [ ] Email Clients: neomutt
- [ ] RSS Clients: newsboat
- [ ] Instant Messaging: telegram, signal
- [ ] Image Viewers: imv
- [ ] Key Remappers:
- [ ] Launchers: onagre
- [ ] Libraries: wlroots wayland-protocols wayland-scanner++ xorg-server-xwayland qt5-wayland kwayland
- [ ] Music Players: mpd, ncmpcpp
- [ ] Notifications:
- [ ] PDF Viewers: zathura
- [ ] Screen Locking:
- [ ] Screencasting:
- [ ] Screenshots:
- [ ] Session Management: seatd
- [ ] Status Bars: yambar luastatus
- [ ] Video Players: mpv
- [ ] Subtitle Editors:
- [ ] Terminal Emulators: foot
- [ ] Text Editor: micro
- [ ] IDE: emacs
- [ ] Notes: nb notion glow
- [ ] Wallpaper:
- [ ] Torrent: rtorrent
- [ ] Office: libreoffice
### Helpers:
- [ ] Audio: audacity ffmpeg
- [ ] Fish: exa
- [ ] Control Version: tig/magit
- [ ] Synchronisation: syncthing chezmoi
- [ ] Automation: CI/CD, entr
- [ ] Virtualisation: KVM
- [ ] Images: Inkscape
- [ ] Writing: Synonimy, Hemingway, Grammarly, Deepl
- [ ] https://github.com/strang1ato/nhi
- [ ] Templates:

<!--## Possibly Asked Questions
- [ ] About electron and PWA.
- [ ] About seatd, elogind and audio.
- [ ] About vi/emacs and micro.
- [ ] About IJKL.
- [ ] About 2 shells and no .

## License
Distributed under the xyz License. See `LICENSE.txt` for more information.

## Contact
Jakub Olszewski - jakubolszewski@tuta.io
Project Link: [https://gitlab.com/jakubolszewski/calm-os/](https://gitlab.com/jakubolszewski/calm-os/)

## Acknowledgments
* []()
* []()
* []()-->

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/github_username/repo_name.svg?style=for-the-badge
[contributors-url]: https://github.com/github_username/repo_name/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/github_username/repo_name.svg?style=for-the-badge
[forks-url]: https://github.com/github_username/repo_name/network/members
[stars-shield]: https://img.shields.io/github/stars/github_username/repo_name.svg?style=for-the-badge
[stars-url]: https://github.com/github_username/repo_name/stargazers
[issues-shield]: https://img.shields.io/github/issues/github_username/repo_name.svg?style=for-the-badge
[issues-url]: https://github.com/github_username/repo_name/issues
[license-shield]: https://img.shields.io/github/license/github_username/repo_name.svg?style=for-the-badge
[license-url]: https://github.com/github_username/repo_name/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/linkedin_username
[product-screenshot]: images/screenshot.png 

<!--<div id="top"></div>
<br />
<div align="center">
  <a href="https://github.com/github_username/repo_name">
    <img src="images/logo.png" alt="Logo" width="80" height="80">
  </a>
<h3 align="center">Calm OS</h3>
  <p align="center">
    project_description
    <br />
    <a href="https://gitlab.com/jakubolszewski/calm-os/"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <!--<a href="https://github.com/github_username/repo_name">View Demo</a>
    ·
    <a href="https://github.com/github_username/repo_name/issues">Report Bug</a>
    ·
    <a href="https://github.com/github_username/repo_name/issues">Request Feature</a>
  </p>
</div>

<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>

## About The Project
[![Product Name Screen Shot][product-screenshot]](https://example.com)
- [x] Name: Calm OS
- [x] Base: Void linux
- [ ] Logo:
- [ ] Motivation:
- [x] Inspirations: BlackBerry OS, Clear Linux, Void Linux, NixOS.
- [ ] Methodologies: Chaos engineering, KISS, SRE automation, cognitive behavioral therapy.
### Built With
## Getting Started
### Prerequisites
### Installation
## Usage
_For more examples, please refer to the [Documentation](https://example.com)_ -->
