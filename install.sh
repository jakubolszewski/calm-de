#!/usr/bin/env bash

set -eu -o pipefail

sudo -n true
test $? -eq 0 || exit 1 "You should have sudo priveledge to run this script."

printf '%b\n' "> Installing curl, gawk, git, bash and sed..."
while read -r p ; do sudo xbps-install -y $p ; done < <(cat << "EOF"
    gawk
    sed
    git
    bash
    curl
EOF
)
printf '%b\n' "> OK"

printf '%b\n' "> Cloning the Calm OS repository..."
git clone https://gitlab.com/jakubolszewski/calm-de /tmp/calm-de
cd /tmp/calm-de || exit
printf '%b\n' "> OK"

printf '%b\n' "> Starting installation..."
bash ./src/main.sh
printf '%b\n' "> Installation done."

printf '%b\n' "> Removing temporary files..."
sudo rm -rf /tmp/calm-os
printf '%b\n' "> Everything done."
