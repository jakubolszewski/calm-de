#!/usr/bin/env bash
set -eu -o pipefail
sudo -n true
test $? -eq 0 || exit 1 "You should have sudo priveledge to run this script."

printf '%b\n' "> Configuring micro text editor..."
export MICRO_TRUECOLOR='1'
export COLORTERM='truecolor'
printf '%b\n' "> OK"
