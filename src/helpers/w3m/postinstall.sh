#!/usr/bin/env bash

set -eu -o pipefail

sudo -n true
test $? -eq 0 || exit 1 "You should have sudo priveledge to run this script."

printf '%b\n' "> Post install script for w3m browser..."

while read -r p ; do sudo xbps-install -y $p ; done < <(cat << "EOF"
    w3m-img
EOF
)

printf '%b\n' "> OK"
