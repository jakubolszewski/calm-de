#!/usr/bin/env bash

set -eu -o pipefail

sudo -n true
test $? -eq 0 || exit 1 "You should have sudo priveledge to run this script."

printf '%b\n' "> Installing Void Linux non free repository..."
while read -r p ; do sudo xbps-install -y $p ; done < <(cat << "EOF"
    void-repo-nonfree
EOF
)
printf '%b\n' "> OK"

printf '%b\n' "> Updating system..."
sudo xbps-install -Syuv
printf '%b\n' "> OK"
