#!/usr/bin/env bash

set -eu -o pipefail

sudo -n true
test $? -eq 0 || exit 1 "You should have sudo priveledge to run this script."

printf '%b\n' "> Installing firmware for Intel CPUs..."
while read -r p ; do sudo xbps-install -y $p ; done < <(cat << "EOF"
    linux-firmware-intel
    mesa-dri
    vulkan-loader
    mesa-vulkan-intel
    libva-vdpau-driver
    intel-video-accel
    xf86-video-intel
    intel-undervolt
    lm_sensors
    acpi_call-dkms
EOF
)
printf '%b\n' "> OK"

printf '%b\n' "> Regenerating initramfs..."
dracut --regenerate-all --force || exit
printf '%b\n' "> OK"
